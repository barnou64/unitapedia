FROM openjdk:8

ENV http_proxy http://cache.univ-pau.fr:3128
ENV https_proxy http://cache.univ-pau.fr:3128
ENV HTTP_PROXY http://cache.univ-pau.fr:3128
ENV HTTPS_PROXY http://cache.univ-pau.fr:3128
ENV NO_PROXY localhost,127.0.0.1,.docker.localhost

ENV PENTAHO_VERSION 9.4.0.0
ENV PENTAHO_BUILD 343
ENV PENTAHO_HOME /pentaho

RUN mkdir -p $PENTAHO_HOME \
    && cd $PENTAHO_HOME \
    && curl -LO https://privatefilesbucket-community-edition.s3.us-west-2.amazonaws.com/$PENTAHO_VERSION-$PENTAHO_BUILD/ce/server/pentaho-server-ce-$PENTAHO_VERSION-$PENTAHO_BUILD.zip \
    && unzip pentaho-server-ce-$PENTAHO_VERSION-$PENTAHO_BUILD.zip \
    && rm pentaho-server-ce-$PENTAHO_VERSION-$PENTAHO_BUILD.zip

EXPOSE 8080 8443

CMD ["/pentaho/start-pentaho.sh"]
