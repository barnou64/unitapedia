FROM eclipse-temurin:21

ENV http_proxy http://cache.univ-pau.fr:3128
ENV https_proxy http://cache.univ-pau.fr:3128
ENV HTTP_PROXY http://cache.univ-pau.fr:3128
ENV HTTPS_PROXY http://cache.univ-pau.fr:3128
ENV NO_PROXY localhost,127.0.0.1,.docker.localhost

RUN mkdir /opt/app
COPY japp.jar /opt/app
CMD ["java", "-jar", "/opt/app/japp.jar"]