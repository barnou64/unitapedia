FROM node:18-alpine

ENV http_proxy http://cache.univ-pau.fr:3128
ENV https_proxy http://cache.univ-pau.fr:3128
ENV HTTP_PROXY http://cache.univ-pau.fr:3128
ENV HTTPS_PROXY http://cache.univ-pau.fr:3128
ENV NO_PROXY localhost,127.0.0.1,.docker.localhost

RUN apk update --repository http://dl-cdn.alpinelinux.org/alpine/v3.19/main --repository http://dl-cdn.alpinelinux.org/alpine/v3.19/community && apk add --no-cache build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev

# Installing libvips-dev for sharp Compatibility
# RUN apk update && apk add --no-cache build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

WORKDIR /opt/
COPY ./unita_dwh_strapi/package.json ./unita_dwh_strapi/yarn.lock ./
RUN yarn config set network-timeout 600000 -g && yarn install

WORKDIR /opt/app
COPY . .
ENV PATH /opt/node_modules/.bin:$PATH

# Create the /opt/build directory and change its ownership to the node user
RUN mkdir -p /opt/build && chown -R node:node /opt/build

# Create the .strapi directory and change its ownership to the node user
RUN mkdir -p /opt/.strapi && chown -R node:node /opt/.strapi

# Change the ownership of the /opt/app directory to the node user
RUN chown -R node:node /opt/app

USER node
RUN ["yarn", "build"]

EXPOSE 1337
CMD ["yarn", "develop"]
