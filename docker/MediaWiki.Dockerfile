# Use the mediawiki:1.39.7 image as the base image
FROM mediawiki:1.39.7

# Set the working directory
WORKDIR /var/www/html

# set proxy for local network (UPPA intranet )
ENV http_proxy http://cache.univ-pau.fr:3128
ENV https_proxy http://cache.univ-pau.fr:3128

# Update the package lists
RUN apt-get update

# Install required packages
RUN apt-get install -y \
    vim \
    unzip \
    libzip-dev \
    wget \
 && docker-php-ext-install zip

# Install composer
RUN curl -sS https://getcomposer.org/installer | php \
 && mv composer.phar /usr/local/bin/composer

# Copy the current directory contents into the container at /var/www/html
COPY . /var/www/html

# Update mediawiki extensions via composer
COPY composer.local.json .
RUN composer self-update 2.1.3 && composer update --no-dev
