# General environment variables (domain name, etc.)
# DOMAIN_NAME: The domain name of your application
DOMAIN_NAME=<your-domain-name>

# MariaDB environment variables (Database)
# MARIADB_USER: The username for the MariaDB database
# MARIADB_PWD: The password for the MariaDB database
# MARIADB_HOST: The hostname for the MariaDB database
# MARIADB_PORT: The port number for the MariaDB database
# MARIADB_DATABASE: The name of the MariaDB database
MARIADB_USER=<your-mariadb-user>
MARIADB_PWD=<your-mariadb-password>
MARIADB_HOST=<your-mariadb-host>
MARIADB_PORT=<your-mariadb-port>
MARIADB_DATABASE=<your-mariadb-database>

# MediaWiki environment variables (Wiki)
# MEDIAWIKI_DB_NAME: The name of the MediaWiki database
# MEDIAWIKI_DB_USER: The username for the MediaWiki database
# MEDIAWIKI_DB_PWD: The password for the MediaWiki database
# MEDIAWIKI_ADMIN_USER: The username for the MediaWiki admin user
# MEDIAWIKI_PWD_EMAIL: The email address for the MediaWiki password reset feature
# MEDIAWIKI_CONTACT_EMAIL: The contact email address for the MediaWiki site
# MEDIAWIKI_SECRET_KEY: A secret key used to secure MediaWiki (should be a random string of 40 characters)
# MEDIAWIKI_ADMIN_PWD: The password for the MediaWiki admin user
# MEDIAWIKI_UPGRADE_KEY: A key used to enable MediaWiki upgrades (should be a random string of 8 characters)
# MEDIAWIKI_API_URL: The URL for the MediaWiki API
MEDIAWIKI_DB_NAME=<your-mediawiki-db-name>
MEDIAWIKI_DB_USER=<your-mediawiki-db-user>
MEDIAWIKI_DB_PWD=<your-mediawiki-db-password>
MEDIAWIKI_ADMIN_USER=<your-mediawiki-admin-user>
MEDIAWIKI_PWD_EMAIL=<your-mediawiki-pwd-email>
MEDIAWIKI_CONTACT_EMAIL=<your-mediawiki-contact-email>
MEDIAWIKI_SECRET_KEY=<your-mediawiki-secret-key> (40 characters)
MEDIAWIKI_ADMIN_PWD=<your-mediawiki-admin-password>
MEDIAWIKI_UPGRADE_KEY=<your-mediawiki-upgrade-key> (8 characters)
MEDIAWIKI_API_URL=<your-mediawiki-api-url>

# Nextcloud environment variables (Cloud storage)
# NEXTCLOUD_DB_NAME: The name of the Nextcloud database
# NEXTCLOUD_DB_USER: The username for the Nextcloud database
# NEXTCLOUD_DB_PWD: The password for the Nextcloud database
# NEXTCLOUD_ADMIN_USER: The username for the Nextcloud admin user
# NEXTCLOUD_ADMIN_PWD: The password for the Nextcloud admin user
# NEXTCLOUD_INSTANCE_ID: A unique identifier for the Nextcloud instance (should be a random string of 32 characters)
# NEXTCLOUD_PWD_SALT: A salt value used to hash passwords in Nextcloud (should be a random string of 32 characters)
# NEXTCLOUD_SECRET: A secret key used to secure Nextcloud (should be a random string of 32 characters)
# NEXTCLOUD_API_URL: The URL for the Nextcloud API
NEXTCLOUD_DB_NAME=<your-nextcloud-db-name>
NEXTCLOUD_DB_USER=<your-nextcloud-db-user>
NEXTCLOUD_DB_PWD=<your-nextcloud-db-password>
NEXTCLOUD_ADMIN_USER=<your-nextcloud-admin-user>
NEXTCLOUD_ADMIN_PWD=<your-nextcloud-admin-password>
NEXTCLOUD_INSTANCE_ID=<your-nextcloud-instance-id> (32 characters)
NEXTCLOUD_PWD_SALT=<your-nextcloud-pwd-salt> (32 characters)
NEXTCLOUD_SECRET=<your-nextcloud-secret> (32 characters)
NEXTCLOUD_API_URL=<your-nextcloud-api-url>

# FTP environment variables (File transfer)
# FTP_USER: The username for the FTP server
# FTP_PWD: The password for the FTP server
# FTP_HOST: The hostname for the FTP server
# FTP_PORT: The port number for the FTP server
# FTP_API_URL: The URL for the FTP API
FTP_USER=<your-ftp-user>
FTP_PWD=<your-ftp-password>
FTP_HOST=<your-ftp-host>
FTP_PORT=<your-ftp-port>
FTP_API_URL=<your-ftp-api-url>

# Pentaho environment variables (Data integration)
# PDI_FTP_HOST: The hostname for the FTP server used by Pentaho
# PDI_FTP_PORT: The port number for the FTP server used by Pentaho
# PDI_FTP_USER: The username for the FTP server used by Pentaho
# PDI_FTP_PWD: The password for the FTP server used by Pentaho
# PDI_API_URL: The URL for the Pentaho API
PDI_FTP_HOST=<your-pdi-ftp-host>
PDI_FTP_PORT=<your-pdi-ftp-port>
PDI_FTP_USER=<your-pdi-ftp-user>
PDI_FTP_PWD=<your-pdi-ftp-password>
PDI_API_URL=<your-pdi-api-url>

# Strapi environment variables (API)
# STRAPI_DB_HOST: The hostname for the Strapi database
# STRAPI_DB_PORT: The port number for the Strapi database
# STRAPI_DB_NAME: The name of the Strapi database
# STRAPI_DB_USER: The username for the Strapi database
# STRAPI_DB_PWD: The password for the Strapi database
# STRAPI_JWT_SECRET: A secret key used to sign JSON Web Tokens in Strapi (should be a random string of 64 characters)
# STRAPI_ADMIN_JWT_SECRET: A secret key used to sign admin JSON Web Tokens in Strapi (should be a random string of 64 characters)
# STRAPI_API_URL: The URL for the Strapi API
# STRAPI_API_TOKEN_SALT: A salt value used to hash API tokens in Strapi (should be a random string of 64 characters)
# STRAPI_API_TOKEN_EXPIRATION_TIME: The expiration time for API tokens in Strapi (e.g. "1h", "24h", etc.)
# STRAPI_ADMIN_EMAIL: The email address for the Strapi admin user
# STRAPI_ADMIN_PASSWORD: The password for the Strapi admin user
# STRAPI_ADMIN_API_TOKEN: The API token for the Strapi admin user
STRAPI_DB_HOST=<your-strapi-db-host>
STRAPI_DB_PORT=<your-strapi-db-port>
STRAPI_DB_NAME=<your-strapi-db-name>
STRAPI_DB_USER=<your-strapi-db-user>
STRAPI_DB_PWD=<your-strapi-db-password>
STRAPI_JWT_SECRET=<your-strapi-jwt-secret> (64 characters)
STRAPI_ADMIN_JWT_SECRET=<your-strapi-admin-jwt-secret> (64 characters)
STRAPI_API_URL=<your-strapi-api-url>
STRAPI_API_TOKEN_SALT=<your-strapi-api-token-salt> (64 characters)
STRAPI_API_TOKEN_EXPIRATION_TIME=<your-strapi-api-token-expiration-time>
STRAPI_ADMIN_EMAIL=<your-strapi-admin-email>
STRAPI_ADMIN_PASSWORD=<your-strapi-admin-password>
STRAPI_ADMIN_API_TOKEN=<your-strapi-admin-api-token>

# Logging environment variables
# LOG_LEVEL: The log level for your application (e.g. "debug", "info", "warn", "error")
LOG_LEVEL=<your-log-level>

# CORS environment variables
# CORS_ORIGIN: The allowed origin for Cross-Origin Resource Sharing (CORS) requests (use "*" to allow all origins)
CORS_ORIGIN=<your-cors-origin>
