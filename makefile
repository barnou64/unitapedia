include .env

EXTENSIONS_CONFIG := extensions.config
EXTENSION_NAMES := $(shell awk '{print $$1}' $(EXTENSIONS_CONFIG))
EXTENSION_VERSIONS := $(shell awk '{print $$2}' $(EXTENSIONS_CONFIG))
BACKUP_DIR=/root/isanumpedia/database_backups

install-mediawiki-no-extension:
	docker compose down
	docker compose build
	docker compose up -d
	sleep 20
	docker compose exec mediawiki composer dump-autoload
	docker compose exec mediawiki composer install

install-mediawiki:
	docker compose down
	rm -rf extensions && mkdir extensions
	@$(foreach name,$(EXTENSION_NAMES), \
		$(eval version := $(word 1, $(EXTENSION_VERSIONS))) \
		echo "Fetching $(name) (version: $(version))"; \
		git clone --branch $(version) https://gerrit.wikimedia.org/r/mediawiki/extensions/$(name) extensions/$(name); \
	)
	git clone https://github.com/StarCitizenTools/mediawiki-extensions-TabberNeue.git extensions/TabberNeue
	cd extensions/VisualEditor &&  git submodule update --init && cd ..
	docker compose build
	docker compose up -d
	sleep 20
	docker compose exec mediawiki composer dump-autoload
	docker compose exec mediawiki composer install


run-mediawiki:
	docker compose down
	docker compose up -d

update-mediawiki-db:
	docker compose exec mediawiki php /var/www/html/maintenance/update.php

database-backup:
	docker compose exec database mysqldump -u $(MARDIADB_USER) -p"$(MARIADB_PWD)" --default-character-set=binary $(MEDIAWIKI_DB_NAME) > $(BACKUP_DIR)/backup_$(shell date +"%d-%m-%Y-%H:%M:%S").sql
	find $(BACKUP_DIR) -type f -name "backup_*.sql" -printf '%T@ %p\n' | sort -n | head -n -7 | cut -d ' ' -f 2- | xargs rm -f

dev-folder:
	rm -rf mediawiki_container_folder && mkdir mediawiki_container_folder
	docker cp isanumpedia_mediawiki_1:/var/www/html/. mediawiki_container_folder/

build: install-mediawiki update-mediawiki-db
build-no-ext: install-mediawiki-no-extension update-mediawiki-db
run: run-mediawiki
dev: dev-folder
update: update-mediawiki-db
backup: database-backup