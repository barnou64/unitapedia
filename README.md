# MAKE commands

## Installer mediawiki

```console
make build
```

## Mettre la base de données mediawiki

```console
make update
```

## Lancer la stack mediawiki

```console
make run
```

## Effectuer un backup

```console
make backup
```

## Copier le contenu du container mediawiki dans le dossier mediawiki_container_folder (aide pour le dév)

```console
make dev
```

## Changer votre logo en gardant le nom logo.svg

- Si vous modifier le nom du logo, il faut éditer le docker-compose.yml et le LocalSettings.php au besoin

# RDF Store documentation (Fuseki)

- [Help:SPARQLStore and Fuseki](https://www.semantic-mediawiki.org/wiki/Help:SPARQLStore/RepositoryConnector/Fuseki)
- [Using SPARQL and RDF stores](https://www.semantic-mediawiki.org/wiki/Help:Using_SPARQL_and_RDF_stores)
- [Repairing data and data structures](https://www.semantic-mediawiki.org/wiki/Help:Repairing_SMW%27s_data)
- [LargeTripleStores](https://www.w3.org/wiki/LargeTripleStores)
- [SPARQL triple-store vendors](https://github.com/SemanticMediaWiki/SemanticMediaWiki/issues/3199)
- [Fuseki : Main Server](https://jena.apache.org/documentation/fuseki2/fuseki-main#fuseki-docker)
- [Fuseki : Docker Tools](https://jena.apache.org/documentation/fuseki2/fuseki-docker.html)
