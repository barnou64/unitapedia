import os
import string
import random

def generate_key(length):
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for _ in range(length))

def main():
    nextcloud_instance_id_length = 16
    nextcloud_pwd_salt_length = 32
    nextcloud_secret_length = 64

    nextcloud_instance_id = generate_key(nextcloud_instance_id_length)
    nextcloud_pwd_salt = generate_key(nextcloud_pwd_salt_length)
    nextcloud_secret = generate_key(nextcloud_secret_length)

    print(f"NEXTCLOUD_INSTANCE_ID={nextcloud_instance_id}")
    print(f"NEXTCLOUD_PWD_SALT={nextcloud_pwd_salt}")
    print(f"NEXTCLOUD_SECRET={nextcloud_secret}")

if __name__ == "__main__":
    main()
