<?php
$CONFIG = array (
  'overwrite.cli.url' => 'https://'. getenv( 'DOMAIN_NAME' ) .'/nextcloud',
  'htaccess.RewriteBase' => '/nextcloud',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps/',
      'writable' => true,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => getenv( 'NEXTCLOUD_INSTANCE_ID' ),
  'passwordsalt' => getenv( 'NEXTCLOUD_PWD_SALT' ),
  'secret' => getenv( 'NEXTCLOUD_SECRET' ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'mysql',
  'version' => '27.1.4.1',
  'dbname' => getenv( 'MEDIAWIKI_DB_NAME' ),
  'dbhost' => 'database:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => getenv( 'MARDIADB_USER' ),
  'dbpassword' => getenv( 'MARIADB_PWD' ),
  'installed' => true,
  'trusted_domains' => 
  array (
    0 => 'localhost',
    1 => getenv( 'DOMAIN_NAME' ),
  ),
  'overwritehost' => getenv( 'DOMAIN_NAME' ),
  'overwritewebroot' => '/nextcloud',
  'trusted_proxies' => 
  array (
    0 => 'nginx',
    1 => getenv( 'DOMAIN_NAME' ),
  ),
  'overwriteprotocol' => 'https',
  'theme' => '',
  'loglevel' => 2,
  'maintenance' => false,
);
